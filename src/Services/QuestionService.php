<?php


namespace Ucc\Services;


use JsonMapper;
use KHerGe\JSON\JSON;

class QuestionService
{
    const QUESTIONS_PATH = __DIR__ . '/../../questions.json';

    private JSON $json;
    private JsonMapper $jsonMapper;

    public function __construct(JSON $json, JsonMapper $jsonMapper)
    {
        $this->json = $json;
        $this->jsonMapper = $jsonMapper;
    }

    public function getRandomQuestions(int $count = 5): array
    {
        $allQuestions = $this->json->decodeFile(self::QUESTIONS_PATH);
        shuffle($allQuestions);
        $userQuestions = array_slice($allQuestions, 0, $count);
        Session::set('questions', $userQuestions);
        return $userQuestions;
    }

    public function getQuestionFromSession(int $id): array
    {
        $questions = Session::get('questions', $userQuestions);
    }

    public function getPointsForAnswer(int $id, string $answer): int
    {
        //TODO: Calculate points for the answer given
    }
}